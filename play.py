#!/usr/bin/python3

import getopt
import sys
import importlib


def main(argv):
    usage = 'play.py -g <gamename>'

    try:
        opts, args = getopt.getopt(argv, "hg:", ["game="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in ("-g", "--game"):
            importlib.import_module(f'{arg}.game')


if __name__ == "__main__":
    main(sys.argv[1:])
