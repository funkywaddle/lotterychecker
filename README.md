# Generic Lottery game number checker # 
Currently includes Powerball and Megamillions. 

You add your picks to {game}/picks.py, and run `./play -g {gamename}` eg `.play.py -g powerball` 

The script will pull the current winning numbers from an rss feed, and compare them to each ticket you list in the picks.py file for that game.

The output per ticket:
```text
Winning Numbers: [2, 13, 32, 33, 48]
Winning Powerball: 22
Your Numbers: (1, 2, 3, 4, 5)
Your Powerball: 9
Matched Numbers: [2]
Matched Powerball: False
Not a Winner!
```
OR
```text
Winning Numbers: [2, 13, 32, 33, 48]
Winning Powerball: 22
Your Numbers: (1, 2, 3, 4, 5)
Your Powerball: 22
Matched Numbers: [2]
Matched Powerball: True
Ticket is worth: 8
```