class Ticket:

    def __init__(self, my_picks, my_special_ball):
        self.jackpot = 0
        self.my_picks = my_picks
        self.my_special_ball = my_special_ball
        self.winning_numbers = []
        self.special_ball = None
        self.special_ball_name = 'Special Ball'
        self.multiplier = None
        self.matched_numbers = []
        self.matched_special_ball = None
        self.winnings = 0
        self.feed = None

    def get_jp_amount(self):
        self.jackpot = self.feed.get_jackpot()

    def get_winning_numbers(self):
        winners = self.feed.get_winning_numbers()
        self.winning_numbers = winners['numbers']
        self.special_ball = winners['special_ball']
        self.multiplier = winners['multiplier']

    def process_ticket(self):
        self.check_numbers()
        self.check_special_ball()
        self.get_jp_amount()
        special_ball_wins = self.get_winnings_with_special_ball(self.jackpot)
        non_special_ball_wins = self.get_winnings_without_special_ball()
        self.process_winnings(special_ball_wins, non_special_ball_wins)

    def check_numbers(self):
        for num in self.my_picks:
            if num in self.winning_numbers:
                self.matched_numbers.append(num)

    def check_special_ball(self):
        self.matched_special_ball = False

        if self.special_ball is not None and self.my_special_ball == self.special_ball:
            self.matched_special_ball = True

    def process_winnings(self, special_ball_wins, non_special_ball_wins):
        wins = non_special_ball_wins

        if self.matched_special_ball:
            wins = special_ball_wins

        self.winnings = wins[self.multiplier][len(self.matched_numbers)]
        self.output_ticket()

    def get_ticket_winnings(self):
        return self.winnings

    def output_ticket(self):
        special_ball = self.special_ball_name
        print(f'Winning Numbers: {self.winning_numbers}')
        print(f'Winning {special_ball}: {self.special_ball}')
        print(f'Your Numbers: {self.my_picks}')
        print(f'Your {special_ball}: {self.my_special_ball}')
        print(f'Matched Numbers: {self.matched_numbers}')
        print(f'Matched {special_ball}: {"True" if self.matched_special_ball else "False"}')
        if self.winnings > 0:
            print(f'Ticket is worth: {self.winnings}')
        else:
            print(f'Not a Winner!')
        print()

    def get_winnings_with_special_ball(self, jp):
        pass

    def get_winnings_without_special_ball(self):
        pass
