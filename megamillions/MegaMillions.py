from lib.Ticket import Ticket
from .Feed import Feed
from .winning_structure import get_winnings_mb, get_winnings_no_mb


class MegaMillions(Ticket):

    def __init__(self, my_picks, my_special_ball):
        super().__init__(my_picks, my_special_ball)
        self.special_ball_name = 'MegaBall'
        self.feed = Feed()
        self.get_winning_numbers()

    def get_winnings_with_special_ball(self, jp):
        return get_winnings_mb(self.jackpot)

    def get_winnings_without_special_ball(self):
        return get_winnings_no_mb()
