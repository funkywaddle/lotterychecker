#! /usr/bin/python3

from .picks import my_picks
from .MegaMillions import MegaMillions

cur_picks = []
cur_pb = 0
winnings = 0

for idx, tkt in enumerate(my_picks):
    cur_picks = tkt['picks']
    cur_pb = tkt['mb']
    pbtkt = MegaMillions(cur_picks, cur_pb)
    pbtkt.process_ticket()
    winnings += pbtkt.get_ticket_winnings()

print("Total Winnings:", winnings)
