def get_winnings_mb(jackpot_cash):
    return {
        2: [4, 8, 20, 400, 20_000, jackpot_cash],
        3: [6, 12, 30, 600, 30_000, jackpot_cash],
        4: [8, 16, 40, 800, 40_000, jackpot_cash],
        5: [10, 20, 50, 1_000, 50_000, jackpot_cash]
    }


def get_winnings_no_mb():
    return {
        2: [0, 0, 0, 20, 1_000, 2_000_000],
        3: [0, 0, 0, 30, 1_500, 3_000_000],
        4: [0, 0, 0, 40, 2_000, 4_000_000],
        5: [0, 0, 0, 50, 2_500, 5_000_000]
    }
