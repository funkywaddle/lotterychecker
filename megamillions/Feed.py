import feedparser


class Feed:

    def __init__(self):
        self.feed = 'https://www.texaslottery.com/export/sites/lottery/rss/tlc_latest.xml'
        self.parsed_feed = feedparser.parse(self.feed)

        self.modifiers = {
            'Billion': 1000000000,
            'Million': 1000000
        }

    def get_winning_numbers(self):
        nums_pre = self.parsed_feed.entries[2].summary
        nums_pre = nums_pre.replace('MegaBall', '-').replace(' Megaplier', '-')
        nums = nums_pre.split(' - ')

        winning_numbers = nums[:5]

        for idx, num in enumerate(nums[:5]):
            winning_numbers[idx] = int(num)

        special_ball = int(nums[5])
        multiplier = int(nums[6])
        return {'numbers': winning_numbers, 'special_ball': special_ball, 'multiplier': multiplier}

    def get_jackpot(self):
        jp_pre = self.parsed_feed.entries[3].summary
        jp_pre = jp_pre[jp_pre.index('Cash'):]
        jp_pre = jp_pre[jp_pre.index(':') + 3:]
        jp_data = jp_pre.split()
        return float(jp_data[0]) * self.modifiers[jp_data[1]]
